FROM php:7.1-apache
MAINTAINER Matthew Gaffen <matt@gaffen.co.uk>

WORKDIR /var/www/html/

# mysql compatability
RUN docker-php-ext-install mysqli

# Enable mod_rewrite
RUN a2enmod rewrite

## Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN chmod a+x /usr/local/bin/composer
RUN composer selfupdate

# Add sudo in order to run wp-cli as the www-data user
RUN apt-get update && apt-get install -y sudo less

# Add WP-CLI
RUN curl -o /bin/wp-cli.phar https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
RUN chmod +x /bin/wp-cli.phar

# Cleanup
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN pecl install xdebug-2.5.0 && docker-php-ext-enable xdebug

# install the PHP extensions we need
RUN set -ex; \
				\
				apt-get update; \
				apt-get install -y \
					libjpeg-dev \
					libpng12-dev \
				; \
				rm -rf /var/lib/apt/lists/*; \
				\
				docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
				docker-php-ext-install gd mysqli opcache

EXPOSE 80
CMD ["apache2ctl", "-D", "FOREGROUND"]

# RUN /sbin/usermod -u 1000 www-data

# CMD ["chown", "www-data", "/var/www/html/.htaccess"]
# CMD ["chown", "www-data", "-R", "/var/www/html/wp-content/uploads"]
# CMD ["chown", "www-data", "-R", "/var/www/html/wp-content/upgrade"]
# CMD ["chown", "www-data", "/var/www/html/wp-content"]
# CMD ["chown", "www-data", "-R", "/var/www/html/wp"]
